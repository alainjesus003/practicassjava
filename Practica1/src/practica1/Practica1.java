
package practica1;

/**
 *
 * @author Alain
 */
public class Practica1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        terreno terreno=new terreno();
        terreno.setAncho(10.50f);
        terreno.setLargo(20.00f);
        
        System.out.println("Perimetro= "+terreno.calcularPerimetro());
        System.out.println("Area= "+terreno.calcularArea());
        
        //objeto construido por argumento
        terreno ter=new terreno(10,20.20f,40.00f);
        
        System.out.println("lo ancho es "+ter.getAncho());
        
        //objeto construido por copia
        terreno ter2=new terreno(terreno);
        
        System.out.println("Perimetro= "+ter2.calcularPerimetro());
        System.out.println("Area= "+ter2.calcularArea());
        
    }
    
}
