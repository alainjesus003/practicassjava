
package practica02;

/**
 *
 * @author Alain
 */
public class Cotizacion {
    private int numCotizacion;
    private String descripcionAuto;
    private float precio;
    private float porcentajeInicial;
    private int plazo;
    
    public Cotizacion(){
        
        this.numCotizacion=0;
        this.descripcionAuto="";
        this.precio=0.0f;
        this.porcentajeInicial=0.0f;
        this.plazo=0;
    }
    
    public Cotizacion (int numCotizacion, String descripcionAuto, float precio, float porcentajeInicial, int plazo){
        
        this.numCotizacion=numCotizacion;
        this.descripcionAuto=descripcionAuto;
        this.precio=precio;
        this.porcentajeInicial=porcentajeInicial;
        this.plazo=plazo;
    }
    
    public Cotizacion(Cotizacion otro){
        this.numCotizacion=otro.numCotizacion;
        this.descripcionAuto=otro.descripcionAuto;
        this.precio=otro.precio;
        this.porcentajeInicial=otro.porcentajeInicial;
        this.plazo=otro.plazo;
    }

    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripcionAuto() {
        return descripcionAuto;
    }

    public void setDescripcionAuto(String descripcionAuto) {
        this.descripcionAuto = descripcionAuto;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajeInicial() {
        return porcentajeInicial;
    }

    public void setPorcentajeInicial(float porcentajeInicial) {
        this.porcentajeInicial = porcentajeInicial;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    public float calcularInicial(){
        float inicial=0.0f;
        inicial=this.precio*this.porcentajeInicial;
        return inicial;
    }
    
    public float calcularTotal(){
        float total=0.0f;
        total=this.precio-(this.precio*this.porcentajeInicial);
        return total;
    }
    
    public float calcularMensual(){
        float mensual=0.0f;
        mensual=(this.precio-(this.precio*this.porcentajeInicial))/this.plazo;
        return mensual;
    }
    
}
