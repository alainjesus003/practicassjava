
package practica02;

/**
 *
 * @author Alain
 */
public class Practica02 {
    public static void main(String[] args) {
        // objeto construido por omision
        Cotizacion cotizacion=new Cotizacion();
        cotizacion.setNumCotizacion(0123);
        cotizacion.setDescripcionAuto("Yaris SX");
        cotizacion.setPrecio(220000.00f);
        cotizacion.setPorcentajeInicial(0.25f);
        cotizacion.setPlazo(36);
        
        System.out.println("Pago Inicial= "+cotizacion.calcularInicial());
        System.out.println("Total a financiar= "+cotizacion.calcularTotal());
        System.out.println("Pago mensual= "+cotizacion.calcularMensual());
        
        //objeto construido por argumento
        Cotizacion cot=new Cotizacion(0123,"Yaris SX",220000.00f,0.25f,36);
        System.out.println("El numero de cotizacion es "+cot.getNumCotizacion());
        
        //objeto construido por copio
        Cotizacion cot2=new Cotizacion(cotizacion);
        
        System.out.println("Pago Inicial= "+cot2.calcularInicial());
        System.out.println("Total a financiar= "+cot2.calcularTotal());
        System.out.println("Pago mensual= "+cot2.calcularMensual());
    }
}