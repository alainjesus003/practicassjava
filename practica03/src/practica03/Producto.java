
package practica03;

/**
 *
 * @author Alain
 */
public class Producto {
    private int codigoProducto;
    private String descripcion;
    private String unidadMedida;
    private float precioCompra;
    private float precioVenta;
    private int cantidadProducto;
    
    public Producto(){
        this.codigoProducto=0;
        this.descripcion="";
        this.unidadMedida="";
        this.precioCompra=0.0f;
        this.precioVenta=0.0f;
        this.cantidadProducto=0;
    }
    
    public Producto(int codigoProducto, String descripcion, String unidadMedida, float precioCompra, float precioVenta, int cantidadProducto){
       this.codigoProducto=codigoProducto;
       this.descripcion=descripcion;
       this.unidadMedida=unidadMedida;
       this.precioCompra=precioCompra;
       this.precioVenta=precioVenta;
       this.cantidadProducto=cantidadProducto;
    }
    
    public Producto(Producto otro){
        this.codigoProducto=otro.codigoProducto;
       this.descripcion=otro.descripcion;
       this.unidadMedida=otro.unidadMedida;
       this.precioCompra=otro.precioCompra;
       this.precioVenta=otro.precioVenta;
       this.cantidadProducto=otro.cantidadProducto;
    }
    
    //metodos

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public float getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(float precioCompra) {
        this.precioCompra = precioCompra;
    }

    public float getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(float precioVenta) {
        this.precioVenta = precioVenta;
    }

    public int getCantidadProducto() {
        return cantidadProducto;
    }

    public void setCantidadProducto(int cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }
    //metodos de comportamiento
    public float calcularPrecioV(){
        float precioV=0.0f;
        precioV=this.precioVenta*this.cantidadProducto;
        return precioV;
    }
    
    public float calcularPrecioC(){
        float precioC=0.0f;
        precioC=this.precioCompra*this.cantidadProducto;
        return precioC;
    }
    
    public float calcularGanancia(){
        float ganancia=0.0f;
        ganancia=(this.precioVenta*this.cantidadProducto)-(this.precioCompra*this.cantidadProducto);
        return ganancia;
    }
    
}
