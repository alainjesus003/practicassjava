
package practica03;

/**
 *
 * @author Alain
 */
public class Practica03 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //generar objeto construido por omision
       Producto producto=new Producto();
        producto.setCodigoProducto(1203);
        producto.setDescripcion("atun de agua");
        producto.setUnidadMedida("piezas");
        producto.setPrecioCompra(10.00f);
        producto.setPrecioVenta(15.00f);
        producto.setCantidadProducto(100);
        
        System.out.println("Precio Venta= "+producto.calcularPrecioV());
        System.out.println("Precio Compra= "+producto.calcularPrecioC());
        System.out.println("Ganancia= "+producto.calcularGanancia());
        
        //generar objeto construido por argumento
        Producto prod=new Producto(1203, "atun de agua", "piezas", 10.00f, 15.00f, 100);
        
        System.out.println("La unidad de medida es "+prod.getUnidadMedida());
        
        //generar objeto construido por copia
        Producto prod2=new Producto(producto);
        
        System.out.println("Precio Venta= "+prod2.calcularPrecioV());
        System.out.println("Precio Compra= "+prod2.calcularPrecioC());
        System.out.println("Ganancia= "+prod2.calcularGanancia());
    }
}
